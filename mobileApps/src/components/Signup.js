import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image } from 'react-native';
// import Logo from './Logo';
// import Form from './Form';
// import { LinearGradient } from 'expo';
import LinearGradient from 'react-native-linear-gradient';


class Signup extends Component {
    render() {
        return (
            <View style={styles.container}>
                    <View>
                    <Text style={styles.headerText}>Register</Text>
                            <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Name" placeholderTextColor = "#808080"
                            selectionColor="#808080"
                            onSubmitEditing={() => this.password.focus()}
                            />
                            <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Email" placeholderTextColor = "#808080"
                            selectionColor="#808080"
                            keyboardType="email-address"
                            />
                            <TextInput style={styles.inputBox}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Password"
                            secureTextEntry={true}
                            placeholderTextColor="#808080"
                            ref={(input) => this.password = input}
                            />
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonText}>Register here</Text>
                    </TouchableOpacity>
                    <View>
                        <Image style={{width: 250, height: 250}} source={require('./img/contoh.jpg')}/>
                    </View>
            </View>
          
        );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor:'#ffffff',
        flex: 1,
        alignItems:'center',
        justifyContent : 'flex-end',
        justifyContent :'center',
    },
    signupTextCont : {
        flexGrow : 1,
        justifyContent : 'center',
        alignItems : 'center',
        paddingVertical:16,
        flexDirection:'row'
    },
    signupText: {
        color:'black',
        fontSize:16
    },
    signupButton : {
        color:'#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    button : {
        width:300,
        backgroundColor:'#e65c00',
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13
        // marginTop: 30
    },
    buttonText : {
        fontSize : 13,
        fontWeight : '400',
        color: '#ffffff',
        textAlign : 'center'
    },
    inputBox : {
        width:300,
        backgroundColor:'rgba(255, 255,255,0.2)',
        borderRadius: 5,
        paddingHorizontal:16,
        paddingVertical:12,
        borderBottomWidth: 1,
        borderBottomColor: '#808080',
        fontSize:13,
        color:'#000',
        marginVertical: 10
    },
    headerText : {
        // marginTop: 100,
        fontSize: 20,
    }
});
export default Signup;