import { StyleSheet } from "react-native";
const styles = StyleSheet.create({

    container: {
        flexGrow: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },

    imageBackground : {
        width: 370,
        height: 150
    },
    Name : {
        justifyContent: 'center',
        alignItems: 'center'
    },

    titleName : {
        fontSize: 30,
        paddingTop: 20,
    },
    textTitle : {
        color: '#A19797',
        paddingTop: 10
    },
    
    Footer : {
        borderTopWidth: 1,
        borderTopColor: '#E5E5E5',
        backgroundColor: 'white',
        // shadowOffset:{ width: 10, height: 10},
        // shadowColor: 'black'
    },
    
    ImageFooter : {
        width: 25,
        height: 25,
        marginTop: 3
    },

    Title : {
        fontSize: 23,
        fontWeight: '500',
        marginTop: 50
    },

    containerBody : {
        paddingLeft: 20
    },

    titleNice : {
        fontSize: 23,
        fontWeight: '500',   
    },

    time : {
        color: '#A19797'
    },

    titleFooter : {
        fontSize: 12,
        color: '#989898'
    },
    // TextPayment:{
    //     padding: 20,
    //     flexDirection: 'row',
    // },

    // TextPayment1: {
    //     marginTop: 5,
    //     paddingLeft: 10,
    //     fontWeight: '500',
    // },

    TextImg : {
        // marginTop: 25,
        marginLeft: 0,
        // flexDirection: 'row'
        alignItems: 'center'
    },

    TextImg1: {
        marginLeft: -10,
        marginTop: 50,
        fontSize: 12,
        color: '#B64132',
    },
    
    TextImage2: {
        fontSize: 50,
        color: 'white',
        textDecorationLine: 'underline',
        fontWeight: 'bold',
    },
    
    TextImage3: {
        fontSize: 15,
        color: 'white',
    },
    
    TextGroovyCash :{
        flex: 1,
        flexDirection: 'row',
        opacity: 0.6,
        backgroundColor: '#ee481d',
        borderWidth: 1,
        borderColor: '#ee481d',
        width: 375,
        height: 55,
        alignSelf: "center"
    },
    TextGroovyCash1: {
        fontSize: 12,
        color: 'white',
    },
    TextGroovyCash2: {
        fontSize: 15,
        color: '#FF8D33',
        fontWeight: 'bold',
    },
    ButtonGroovyCash: {
        width: 70, 
        height: 30, 
        marginLeft: 170,
        marginTop: 14, 
        backgroundColor:'#FF8D33',
        borderRadius: 5,
    },
    ButtonGroovyCash1: {
        marginLeft: 12, 
        color: "white",
    },
    // BoxPayment: {
    //     width: 300,
    //     height: 127,
    //     backgroundColor: '#FFF',
    //     marginTop: -50,
    //     borderRadius: 7,
    //     shadowColor: '#000',
    //     shadowOpacity: 0.25,
    //     shadowRadius: 3.84,
    //     elevation: 5
    // }
    
});
export default styles;