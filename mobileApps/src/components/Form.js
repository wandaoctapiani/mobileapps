import React, { Component } from 'react';
import { StyleSheet, View, CheckBox, Text, TextInput, TouchableOpacity } from 'react-native';
import {Actions,Router, Scene,} from 'react-native-router-flux';

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
			isChecked: {},
		};

    }

    render() {
        return (
                <View style={styles.container}>

                        <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Email / Nomor HP" placeholderTextColor = "#808080"
                        selectionColor="#808080"
                        keyboardType="email-address"
                        onSubmitEditing={() => this.password.focus()}
                        />

                        <TextInput style={styles.inputBox}
                        underlineColorAndroid="rgba(0,0,0,0)"
                        placeholder="Password"
                        secureTextEntry={true}
                        placeholderTextColor="#808080"
                        ref={(input) => this.password = input}
                        />

                        <View style={{flex : 1, flexDirection: 'row'}}>
                            <CheckBox 
                                title='Remember me'
                                checkedColor='black'
                                checked={this.state.checked}
                                onPress={() => this.setState({checked: this.state.checked})}
                            />

                            <Text style={{color: '#FF0000', marginTop: 5, fontSize: 12}}>
                                Lupa Kode Keamanan?
                            </Text>

                        </View>  

                    <TouchableOpacity style={styles.button} onPress={Actions.Homepage}>
                        <Text style={styles.buttonText}>{this.props.type}</Text>
                    </TouchableOpacity>

            </View>
           
        );
    }
}

const styles = StyleSheet.create({
    
    container : {
        flexGrow : 1,
        justifyContent : 'center',
        alignItems : 'center'
    },
    inputBox : {
        width:300,
        backgroundColor:'rgba(255, 255,255,0.2)',
        borderRadius: 5,
        paddingHorizontal:16,
        paddingVertical:12,
        borderBottomWidth: 1,
        borderBottomColor: '#808080',
        fontSize:13,
        color:'#000',
        marginVertical: 10
        
    },
    button : {
        width:300,
        // backgroundColor:'#e65c00',
        backgroundColor: '#FD7102',
        borderRadius: 5,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText : {
        fontSize : 16,
        fontWeight : '500',
        color: '#ffffff',
        textAlign : 'center'
    }
});
export default Form;