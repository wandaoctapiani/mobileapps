import React, { Component } from 'react';
import { ImageBackground, Alert, Platform, Text, StatusBar, StyleSheet, View, Image, ScrollView, TextInput, KeyboardAvoidingView, TouchableOpacity, TouchableHighlight } from "react-native";
import { Actions, Router, Scene } from 'react-native-router-flux';
import { Container,
         Header,
         Title,
         Content,
         Button,
         Icon,
         Left,
         Right,
         Body,
         Badge,
         Footer,
         FooterTab

} from "native-base";
import { Svg } from "react-native-svg";
import {CheckBox, FormValidationMessage, FormInput, FormLabel, leftComponent} from "react-native-elements";
import styles from "./HomeStyle";

class Homepage extends Component {
    constructor(){
        super()
        this.state = {
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
        };
    }

    toggleTab1(){
        this.setState({
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
        });
    }

    toggleTab2(){
        this.setState({
            tab1: true,
            tab2: false,
            tab3: false,
            tab4: false,
        });
    }

    toggleTab3(){
        this.setState({
            tab1: false,
            tab2: false,
            tab3: true,
            tab4: false,
        });
    }
    
    toggleTab4(){
        this.setState({
            tab1: false,
            tab2: false,
            tab3: false,
            tab4: true,
        });
    }

    render() {
        return (
            <Container>
                {/* <ImageBackground source={require('../components/img/img-background.png')}
                    style={styles.imageBackground}>
                    <View style={styles.TextImg}>
                        <Text style={styles.TextImg1}>Good Morning Wanda!</Text>
                    </View>
                </ImageBackground> */}
                <View style={styles.container}>
                    <View style={styles.Name}>
                        <Text style={styles.titleName}>Hai Wanda,</Text>
                        <Text style={styles.textTitle}>Your current cycle ends in 8 days</Text>
                    </View>
                    {/* <View style={styles.containerBody}>
                        <Text style={styles.Title}>You have unlimited data.</Text>
                        <Text style={styles.titleNice}>Nice!</Text>
                        <Text style={styles.time}>You have 2.58 GB so far</Text>
                    </View> */}
                    {/* <View style={styles.BoxPayment}>
                        <View style={styles.TextPayment}>
                            <Svg>
                                <Image source={require('./img/credit-cards-payment.svg')}/>
                            </Svg>
                            <Icon name='card'/>
                            <Text style={styles.TextPayment1}>Payment</Text>
                            <View style={{flexDirection: 'row', marginLeft: 45}}>
                                <Text style={{paddingLeft: 50, fontSize: 11, lineHeight: 18, color: '#000'}}>RP</Text>
                                <Text style={{fontSize: 20, lineHeight: 30, color: '#000', fontWeight: 'bold'}}>13.757</Text>
                            </View>
                        </View>
                    </View> */}
                </View>

                <Footer>
                    <FooterTab style={styles.Footer}>
                        {/* Home footer */}
                        <Button 
                            active={this.state.tab1} 
                            onPress={() => {Actions.Homepage}}
                            vertical
                            badge
                        >
                            {/* <Badge>
                                <Text style={{marginBottom: 0, alignSelf: 'center'}}>100</Text>
                            </Badge> */}
                            <Image style={styles.ImageFooter} source={require('./img/home.png')}/>
                            <Text style={styles.titleFooter}>Home</Text>
                        </Button>
                        {/* Finance footer */}
                        <Button active={this.state.tab2} onPress={Actions.homescreen}>
                            <Image 
                                style={styles.ImageFooter}
                                source={require('./img/bag.png')}
                            />
                            <Text style={styles.titleFooter}>Finance</Text>
                        </Button>
                        {/* Support footer */}
                        <Button>
                            <Image 
                                style={styles.ImageFooter}
                                source={require('./img/support.png')}
                            />
                            <Text style={styles.titleFooter}>Support</Text>
                        </Button>
                        {/* Support footer */}
                        <Button>
                            <Image 
                                style={styles.ImageFooter}
                                source={require('./img/technical-support.png')}
                            />
                            <Text style={styles.titleFooter}>Informasi</Text>
                        </Button>
                        {/* Support footer */} 
                        <Button>
                            <Image 
                                style={styles.ImageFooter}
                                source={require('./img/user.png')}
                            />
                            <Text style={styles.titleFooter}>Account</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export default Homepage;

// 50114
