import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Logo from "./Logo";
import Form from "./Form";

class Login extends Component {

    signup() {

        Actions.signup()
        
        }
    render() {
        return (
            <View style={styles.container}>
                <Logo/>
                <ScrollView>
                
                    <Form type="Login"/>
                    <View style={styles.signupTextCont}>
                        <Text style={styles.signupText}>Belum punya akun?</Text>
                        <TouchableOpacity onPress={this.signup}>
                            {/* <Text style={styles.signupButton}>Signup</Text> */}
                            <Text style={{textDecorationLine: "underline", textDecorationStyle: 'solid', color: '#0075FF', marginLeft: 5, fontSize: 13}}>Daftar Disini</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
              
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container : {
        // backgroundColor:'#cbad6d',
        backgroundColor: '#fff',
        flex: 1,
        alignItems:'center',
        justifyContent :'center',
    },
    signupTextCont : {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:16,
        flexDirection:'row'
    },
    signupText : {
        color: '#8D8A8A',
        // color:'rgba(255,255,255,0.6)',
        fontSize:12
    },
    signupButton : {
        color:'#ffffff',
        fontSize:14,
        fontWeight:'500'
    }
});
export default Login;