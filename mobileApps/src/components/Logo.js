import React, { Component } from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';

class Logo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={{width: 163, height: 50 }} source={require('./img/groovy.png')}/>
                <Text style={styles.logoText}> Welcome To my App </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    
    container : {
        flexGrow : 1, 
        justifyContent : 'flex-end',
        alignItems : 'center',
    },
    logoText : {
        marginVertical : 15,
        fontSize : 10,
        color : 'rgba(255, 255, 255, 0.7)'
    }
});
export default Logo;