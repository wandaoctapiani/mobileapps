import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import Login from '../components/Login';
import Signup from '../components/Signup';
import HomeScreenFirst from "../components/HomeScreenFirst";
import Homepage from "../components/Homepage";


export default class Routes extends Component {
    render() {
        return(

        <Router> 
            <Stack key="root" hideNavBar={true}>
                <Scene key="login" component={Login} title="Login" initial={true}/>
                <Scene key="signup" component={Signup} title="Register"/>
                <Scene key="homescreen" component={HomeScreenFirst} title="HomeScreen"/>
                <Scene key="Homepage" component={Homepage} title="Homepage"/>
            </Stack>    
        </Router>
        )
    }
}