import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
// import './src/components/style.css';


import Routes  from "./src/routes/index";

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#1c313a" barStyle="light-content"/>
      
      <Routes />
      {/* <Text>Welcome back Wanda</Text> */}
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
